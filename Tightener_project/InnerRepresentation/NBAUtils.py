'''
    Copyright (c) 2023 Marek Jankola

    This file is part of Tightener.

    Tightener is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tightener is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tightener.  If not, see <http://www.gnu.org/licenses/>.
'''

from InnerRepresentation.NBARepresentation import *
from InnerRepresentation.InternalUtils import *


def create_nba_representation(nba: spot.twa_graph) -> NBA_representation:
    states : Set[composite_state]
    init_states : Set[composite_state]
    final_states : Set[composite_state]

    states = {composite_state(list(),[str(nba.get_init_state_number())],False)}
    final_states = set()
    transitions: dict[composite_state, Set[Tuple[composite_state, buddy.bdd, bool]]] = {}
    init_states = {composite_state(list(),[str(nba.get_init_state_number())],False)}

    for s in range(0, nba.num_states()):
        new_state = composite_state(list(),[str(s)],False)
        states.add(new_state)

        for t in nba.out(s):
            if new_state not in transitions.keys():
                transitions[new_state] = set()
            transitions[new_state].add((composite_state(list(),[str(t.dst)],False), t.cond, t.acc.count() != 0))
            states.add(composite_state(list(),[str(t.dst)],False))
           
    return NBA_representation(states, init_states, final_states, transitions)

# Creates the initial component that will be used further in the composition.
def create_initial_C(nba: Internal_represenatation, is_hat: bool) -> NBA_representation:
    states : Set[composite_state]
    init_states : Set[composite_state]
    final_states : Set[composite_state]

    states = set()
    final_states = set()
    transitions: dict[composite_state, Set[Tuple[composite_state, buddy.bdd, bool]]] = {}
    init_states = set()

    for state in nba.init_states:
        init_states.add(composite_state([],[str(state)],False))

    visited = set()
    not_visited = set()

    for s in nba.states:
        not_visited.add(composite_state([],[str(s)],False))
    
    while len(not_visited) != len(visited):
        new_state = not_visited.pop()
        not_visited.add(new_state)
        visited.add(new_state)

        states.add(new_state)
        
        if str(new_state.loop_states[0]) not in nba.transitions.keys():
            continue
        
        for (dst, cond, acc) in nba.transitions[str(new_state.loop_states[0])]:
            if new_state not in transitions.keys():
                transitions[new_state] = set()
            if (is_hat & (acc | new_state.is_marked)):
                compos_dst = composite_state(list(),[str(dst)],True)
            else:
                compos_dst = composite_state(list(),[str(dst)],False)
            transitions[new_state].add((compos_dst, cond, False))
            not_visited.add(compos_dst)
            
    return NBA_representation(states, init_states, final_states, transitions) 