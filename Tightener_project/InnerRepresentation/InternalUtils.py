'''
    Copyright (c) 2023 Marek Jankola

    This file is part of Tightener.

    Tightener is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tightener is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tightener.  If not, see <http://www.gnu.org/licenses/>.
'''

from InnerRepresentation.NBARepresentation import *
from typing import Dict, Set, Tuple

# Internal representation of automata, so that we can easier and faster parse the information
class Internal_represenatation:
    def __init__(self, states: Set[str], init_states: Set[str], final_states: Set[str], transitions: Dict[str, Set[Tuple[str, buddy.bdd, bool]]]) -> None:
        self.states : Set[str] = states 
        self.init_states : Set[str] = init_states
        self.final_states : Set[str] = final_states
        self.transitions : Dict[str, Set[Tuple[str, buddy.bdd, bool]]] = transitions
    
    def copy(self):
        return Internal_represenatation(self.states.copy(), self.init_states.copy(), self.final_states.copy(), self.transitions.copy())
    
    def get_single_init_state(self):
        for i in self.init_states:
            return i

def create_internal(nba: spot.twa_graph) -> Internal_represenatation:
    states : Set[str]
    init_states : Set[str]
    final_states : Set[str]

    states = set()
    final_states = set()
    transitions: dict[str, Set[Tuple[str, buddy.bdd, bool]]] = {}
    init_states = {str(nba.get_init_state_number())}

    for s in range(0, nba.num_states()):
        states.add(str(s))
        #if nba.state_is_accepting(s):
        #    final_states.add(str(s))

        for t in nba.out(s):
            if str(s) not in transitions.keys():
                transitions[str(s)] = set()
            transitions[str(s)].add((str(t.dst), t.cond, t.acc.count() != 0))
            
    return Internal_represenatation(states, init_states, final_states, transitions)


def reduce_using_simulation_clemente(init_states : Set[str], init_state_nba : int, nba : spot.twa_graph, mapping : Dict[str, int]) -> Tuple[spot.twa_graph, Set[str]]:
    # Since Spot does not support more than one initial state, we connect the disjunct components (starting by different initial states) by fresh Ap: init
    # We do it to make them reachable, so that the reduction algorithm does not erase them.
    init_ap_index = nba.register_ap("init")
    new_ap = buddy.bdd_ithvar(init_ap_index)

    for state in init_states:
        if mapping[state] != init_state_nba:
            nba.new_edge(init_state_nba, mapping[state], new_ap, [1,2,3])

    if nba.num_states() == 0:
        nba.new_state()
        nba.set_init_state(0)
        return nba, set()
    
    reduced_nba = spot.reduce_direct_sim_sba(nba)
    init_edges, new_init_states = collect_init_edges(reduced_nba)
    remove_edges(reduced_nba, init_edges, init_ap_index)


    return reduced_nba, new_init_states

def reduce_using_simulation_babiak(init_states : Set[str], init_state_nba : int, nba : spot.twa_graph, mapping : Dict[str, int]) -> Tuple[spot.twa_graph, Set[str]]:
    # Since Spot does not support more than one initial state, we connect the disjunct components (starting by different initial states) by fresh Ap: init
    # We do it to make them reachable, so that the reduction algorithm does not erase them.
    # 
    # !!! Transforms TBA to BA and might violate tightness !!! Do not use unless result validation afterwards

    init_ap_index = nba.register_ap("init")
    new_ap = buddy.bdd_ithvar(init_ap_index)

    for state in init_states:
        if mapping[state] != init_state_nba:
            nba.new_edge(init_state_nba, mapping[state], new_ap, [1,2,3])
    reduced_nba = spot.simulation_sba(nba)
    init_edges, new_init_states = collect_init_edges(reduced_nba)
    remove_edges(reduced_nba, init_edges, init_ap_index)

    return reduced_nba, new_init_states


def remove_edges(nba : spot.twa_graph, edges : Set[int], ap_init):
    for state in range(nba.num_states()):
        oi = nba.out_iteraser(state)
        while oi:
            n = nba.edge_number(oi.current())
            if n in edges:
                oi.erase()
            else:
                oi.advance()
    nba.unregister_ap(ap_init)

def collect_init_edges(nba : spot.twa_graph) -> Tuple[Set[int], Set[int]]:
    init_edges = set()
    init_states = set()

    for i in range(0, nba.num_edges()):
        if nba.edge_storage(i).acc.count() == 3:
            init_edges.add(i)
            init_states.add(nba.edge_storage(i).src)
            init_states.add(nba.edge_storage(i).dst)
    return init_edges, init_states

# The implementation of the construction described in Section 4.1
def convert_to_state_acc(nba : NBA_representation) -> NBA_representation:
    nba2 = NBA_representation(set(), set(), set(), {})
    # Map that we use to keep a track of which state is copy of which
    map_new_states = {}
    new_states = set()
    for src in nba.transitions.keys():
        nba2.states.add(src.copy())
        if src in nba.init_states:
            nba2.init_states.add(src.copy())
        if src.copy() not in nba2.transitions:
            nba2.transitions[src.copy()] = set()

        for (dst, symbol, acc) in nba.transitions[src]:
            nba2.states.add(dst.copy())

            if dst in nba.init_states:
                nba2.init_states.add(dst.copy())

            if not(acc):
                nba2.transitions[src.copy()].add((dst.copy(), symbol, False))
            
            if dst not in nba.transitions.keys():
                continue

            for (dst2, symbol2, acc2) in nba.transitions[dst]:
                if acc2:
                    new_state = dst.copy()
                    # We have to distinguish between original states and their copies as described in Section 4.1,
                    # so we give them new names ending with suffix 'XXX'.
                    new_state.add_stem_state("XXX")
                    map_new_states[dst] = new_state
                    new_states.add(new_state)
                    nba2.states.add(new_state)
                    if dst in nba.init_states:
                        nba2.init_states.add(new_state)

                    if new_state not in nba2.transitions:        
                        nba2.transitions[new_state] = set()
                    nba2.transitions[src.copy()].add((new_state, symbol, False))
                    
                    if (dst2 == dst):
                        nba2.transitions[new_state].add((new_state, symbol2, True))
                    else:
                        nba2.transitions[new_state].add((dst2.copy(), symbol2, True))
    
    for src in nba2.states:
        if (src in nba2.init_states) and (src in map_new_states):
            nba2.init_states.add(map_new_states[src])
        new_dests = set()
        if src not in nba2.transitions:
            continue
        for (dst, symbol, acc) in nba2.transitions[src]:
            if ((src in new_states) and (dst in map_new_states)):
                new_dests.add((map_new_states[dst], symbol, True))
        nba2.transitions[src] = nba2.transitions[src].union(new_dests)

    return nba2
    

def create_spot(nba_repre: NBA_representation, print_init_states=True, with_reduction_clemente=False, 
                with_reduction_babiak=False, state_acceptance=False) -> spot.twa_graph:
    mapping : Dict[composite_state, int] = {}
    init_state_index : int  = 0
    init_states : Set[str] = set()

    nba = spot.make_twa_graph()
    nba.set_buchi()
    if (state_acceptance):
        nba.prop_state_acc(True)
        nba_repre = convert_to_state_acc(nba_repre)
    nba.new_states(len(nba_repre.states))

    i = 0
    for state in nba_repre.states:
        mapping[state] = i
        i += 1

    for state in nba_repre.init_states:
        init_states.add(mapping[state])
        nba.set_init_state(mapping[state])
        init_state_index = mapping[state]
    
    for src in nba_repre.transitions.keys():
        for (dest, symbol, acc) in nba_repre.transitions[src]:
            if acc:
                nba.new_edge(mapping[src], mapping[dest], symbol, [0])
            else:
                nba.new_edge(mapping[src], mapping[dest], symbol)

    if with_reduction_clemente:
        return reduce_using_simulation_clemente(nba_repre.init_states, init_state_index, nba, mapping)
    
    if with_reduction_babiak:
        return reduce_using_simulation_babiak(nba_repre.init_states, init_state_index, nba, mapping)
    return nba, init_states