'''
    Copyright (c) 2023 Marek Jankola

    This file is part of Tightener.

    Tightener is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tightener is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tightener.  If not, see <http://www.gnu.org/licenses/>.
'''

from InnerRepresentation.CompositeState import *
from typing import Dict, Set, Tuple

# Representation of the tight automaton for the construction in Section 4.
class NBA_representation:
    def __init__(self, states: Set[composite_state], init_states: Set[composite_state], final_states: Set[composite_state], 
                 transitions: Dict[composite_state, Set[Tuple[composite_state, buddy.bdd, bool]]]) -> None:
        self.states : Set[composite_state] = states 
        self.init_states : Set[composite_state] = init_states
        self.final_states : Set[composite_state] = final_states
        self.transitions : Set[composite_state] = transitions

    def accepts_empty(self) -> bool:
        return len(self.init_states) == 0
    
    def copy(self):
        return NBA_representation(self.states.copy(), self.init_states.copy(), self.final_states.copy(), self.transitions.copy())
    
    def get_single_init_state(self):
        for i in self.init_states:
            return i
        
    def get_single_final_state(self):
        for i in self.final_states:
            return i
        
    def unmark_all_states(self):
        for state in self.states:
            state.unmark_state()

    def contains_marked_states(self):
        for state in self.states:
            if state.is_marked:
                return True
        return False

    def reduce(self) -> None:
        final_st = set()
        for src in self.transitions:
            for (dst, cond, acc) in self.transitions[src]:
                if acc:
                    final_st.add(src)

        can_reach_final = self.set_is_reachable_from(final_st)
        #init_can_reach = self.set_can_reach(self.init_states)
        curr = can_reach_final
        
        if can_reach_final == 0:
            self.states = set()
            self.init_states = set()
            self.final_states = set()
            self.transitions = {}
            return
        
        self.states = curr
        self.final_states = can_reach_final
        self.init_states = can_reach_final
        new_transitions = {}

        for src in self.transitions.keys():
            if src not in curr:
                continue
            succ = set()
            for (dest, a, b) in self.transitions[src]:
                if dest not in curr:
                    continue
                succ.add((dest, a, b))
            if len(succ) == 0:
                continue
            new_transitions[src] = succ
        
        self.transitions = new_transitions

    #computes a set of states from which an input set of states is reachable
    def set_is_reachable_from(self, init_set: Set[str]) -> Set[str]:
        if init_set.intersection(self.states) != init_set:
            return set()
        
        prev = set()
        curr = init_set.copy()
        while prev != curr:
            prev = curr.copy()
            for src in self.transitions.keys():
                for (dest, a, b) in self.transitions[src]:
                    if str(dest) in curr:
                        curr.add(str(src))
        
        return curr
    
    #computes a set of states that are reachable from an input set of states
    def set_can_reach(self, init_set: Set[composite_state]) -> Set[composite_state]:
        if init_set.intersection(self.states) != init_set:
            return set()
        
        prev = set()
        curr = init_set.copy()
        while prev != curr:
            prev = curr.copy()
            for src in self.transitions:
                if src in prev:
                    for (dest, a, b) in self.transitions[src]:
                        curr.add(dest)
        return curr

    def compute_SCCs(self) -> List[Set[str]]:
        SCCs = list()
        found = set()
        already_explored : Dict[str, Set[str]] = {}
        for state in self.states:
            if state in found:
                continue
            SCC = set({state})
            found.add(state)
            if state not in already_explored.keys():
                already_explored[state] = self.set_is_reachable_from({state})
            
            for src in already_explored[state]:
                if src not in already_explored.keys():
                    already_explored[src] = self.set_is_reachable_from({src})
                if state in already_explored[src]:
                    SCC.add(src)
                    found.add(src)
            SCCs.append(SCC)
        return SCCs