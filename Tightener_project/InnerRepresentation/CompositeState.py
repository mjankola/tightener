'''
    Copyright (c) 2023 Marek Jankola

    This file is part of Tightener.

    Tightener is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tightener is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tightener.  If not, see <http://www.gnu.org/licenses/>.
'''

import spot
import buddy
import copy
from typing import List

class composite_state:
    def __init__(self, stem_states: List[str], loop_states: List[str], is_marked: bool) -> None:
        self.stem_states : List[str] = stem_states 
        self.loop_states : List[str] = loop_states
        self.is_marked : bool = is_marked

    def __hash__(self):
        return hash(self.get_string_representation())

    def __eq__(self, other):
        return (self.get_string_representation() == other.get_string_representation())

    def __ne__(self, other):
        # Not strictly necessary, but to avoid having both x==y and x!=y
        # True at the same time
        return not(self == other)
    
    def copy(self):
        return composite_state(copy.deepcopy(self.stem_states), copy.deepcopy(self.loop_states), self.is_marked)
    
    # Checks if the state is not already present in stem or loop states (with the only exception of the last state)
    def is_not_valid_loop_state(self, state: str) -> bool:
        if len(self.stem_states) == 0:
            return (str(state) in self.loop_states)
        return ((str(state) in self.loop_states) or ((str(state) in self.stem_states) and (str(state) != str(self.stem_states[-1]))))
    
    # Checks if a given composite state is valid successor. In other words, if the last stem state and the last loop state
    # of this composite state equal, then its successor's states must also equal. 
    def is_not_valid_successor(self, succ) -> bool:
        if len(self.stem_states) == 0:
            return False
        if (str(self.stem_states[-1]) != str(self.loop_states[-1])):
            return False
        return (str(succ.stem_states[-1]) == str(succ.loop_states[-1]))
    
    # Checks if the state is not already present in stem or loop states (with the only exception of the last state)
    def is_not_valid_stem_state(self, state: str) -> bool:
        return ((str(state) in self.stem_states) or ((str(state) in self.loop_states) and (str(state) != str(self.loop_states[-1]))))
    
    # Checks if the last stem and loop states are not the same. If yes then it does not make sense to increase number of states in it.
    def is_not_extensible(self) -> bool:
        if len(self.stem_states) == 0:
            return False
        return (self.stem_states[-1] == self.loop_states[-1])

    def get_string_representation(self) -> str:
        state_str = ','.join(self.stem_states)
        state_str += '['
        state_str += ','.join(self.loop_states)
        state_str += ']'
        if self.is_marked:
            state_str += '#'
        return state_str
    
    def get_first_state(self) -> str:
        if len(self.stem_states) > 0 :
            return self.stem_states[0]
        else:
            return self.loop_states[0]

    def add_stem_state(self, state: str):
        self.stem_states.append(str(state))

    def add_loop_state(self, state: str):
        self.loop_states.append(str(state))
    
    def set_marked_state(self, index: int):
        self.marked_index = index

    def mark_state(self):
        self.is_marked = True

    def unmark_state(self):
        self.is_marked = False