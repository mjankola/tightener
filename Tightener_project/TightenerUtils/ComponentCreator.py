'''
    Copyright (c) 2023 Marek Jankola

    This file is part of Tightener.

    Tightener is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tightener is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tightener.  If not, see <http://www.gnu.org/licenses/>.
'''

from InnerRepresentation.NBAUtils import *

# Creates enclosing edge, in other words, constructs the transitions of the type (6) and (7) from the text.
def add_enclosing(s : composite_state, r : composite_state, 
                  original_nba : Internal_represenatation, C : NBA_representation) -> bool:
    if (s.is_marked):
        return False
    
    if (r != s and ((len(s.stem_states) > 0) and (str(s.stem_states[-1]) == str(s.loop_states[-1])))):
        return False

    if len(s.loop_states) == 1:
        init0 = r.loop_states[0]
        init1 = s.loop_states[0]
    else:
        init0 = r.loop_states[0]
        init1 = s.loop_states[1]

    if str(init0) not in original_nba.transitions:
        return False
    
    edge = get_condition_of_edge(original_nba.transitions[str(init0)], str(init1))
    if edge == None:
        return False
    (cond, acc) = edge
    
    for i in range(len(r.stem_states)):
        if str(r.stem_states[i]) not in original_nba.transitions:
            return False
        if i < (len(r.stem_states) - 1):
            edge = get_condition_of_edge(original_nba.transitions[r.stem_states[i]], str(s.stem_states[i+1]))
            if edge == None:
                return False
            (cond2, acc2) = edge
            if buddy.bdd_and(cond, cond2) == buddy.bddfalse:
                return False
            cond = buddy.bdd_and(cond, cond2)
            acc = acc and acc2
        else:
            edge = get_condition_of_edge(original_nba.transitions[r.stem_states[i]], str(s.loop_states[0]))
            if edge == None:
                return False
            (cond2, acc2) = edge
            if buddy.bdd_and(cond, cond2) == buddy.bddfalse:
                return False
            cond = buddy.bdd_and(cond, cond2)
            acc = acc or acc2

    for i in range(len(r.loop_states)):
        if str(r.loop_states[i]) not in original_nba.transitions:
            return False
        if i < (len(r.loop_states) - 1):
            edge = get_condition_of_edge(original_nba.transitions[r.loop_states[i]], str(s.loop_states[i+1]))
            if edge == None:
                return False
            (cond2, acc2) = edge
            if buddy.bdd_and(cond, cond2) == buddy.bddfalse:
                return False
            cond = buddy.bdd_and(cond, cond2)
            acc = acc and acc2
        else:
            edge = get_condition_of_edge(original_nba.transitions[r.loop_states[i]], str(s.loop_states[0]))
            if edge == None:
                return False
            (cond2, acc2) = edge
            if buddy.bdd_and(cond, cond2) == buddy.bddfalse:
                return False
            cond = buddy.bdd_and(cond, cond2)
            acc = acc and acc2


    if (r.is_marked or (acc and (not(s.is_marked) and not(r.is_marked)))):
        if r not in C.transitions:
            C.transitions[r] = set()
        # If the condition on the constructing edge is implied by conditions in original automaton
        # we do not have to add it
        if cond != buddy.bddfalse:
            C.transitions[r].add((s, cond, True))
        return True
    return False

def get_condition_of_edge(successors : Set[Tuple[str, buddy.bdd]], dst : str) -> Tuple[buddy.bdd, bool]:
    for (dst2, cond, acc) in successors:
        if str(dst2) == str(dst):
            return (cond, acc)
    return None

# Creates transition of type (3) from the text. In other words, includes a new tight component into the original automaton.
def add_tight_component(Ct : NBA_representation, tight_component : NBA_representation, original_nba : Internal_represenatation):
    for state in Ct.states:
        tight_component.states.add(state)
    
    for src in Ct.transitions:
        tight_component.transitions[src] = Ct.transitions[src]
    
    for state in Ct.states:
        if str(state.get_first_state()) in original_nba.init_states:
            tight_component.init_states.add(state)
        
        for src in original_nba.transitions:
            for (dst, cond, acc) in original_nba.transitions[src]:
                if str(dst) == str(state.get_first_state()):
                    tight_component.transitions[composite_state(set(), [src], False)].add((state, cond, False))