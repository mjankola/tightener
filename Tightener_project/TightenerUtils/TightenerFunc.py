'''
    Copyright (c) 2023 Marek Jankola

    This file is part of Tightener.

    Tightener is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tightener is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tightener.  If not, see <http://www.gnu.org/licenses/>.
'''

from TightenerUtils.Composition import *
from TightenerUtils.ComponentCreator import *

# Main procedure for the construction of the tight automaton described in Section 4.
def Tighten_TBA(B : spot.twa_graph) -> NBA_representation:
    tight_ba = create_nba_representation(B)
    internal_ba = create_internal(B)
    init_hat_C = create_initial_C(internal_ba, True)
    init_nonhat_C = create_initial_C(internal_ba, False)
    prev_nonhat_C = init_nonhat_C.copy()
    prev_hat_C = set()
    prev_hat_C.add(init_hat_C)
    prev_stem_hat_C = prev_hat_C
    prev_stem_nonhat_C = init_hat_C

    for i in range(0, B.num_states()):
        for k in range(1, B.num_states()):
            if (((k + i > 1) and k != 0) and ((i+k) <= (B.num_states()))):
                for orig_C in prev_hat_C:
                    C = orig_C.copy()
                    for s in C.states:
                        for r in C.set_can_reach({s}):
                            add_enclosing(s, r, internal_ba, C)
                    add_tight_component(C, tight_ba, internal_ba)
            new_hat_C = set()
            for C in prev_hat_C:
                new_hat_C.add(PSC_loop(internal_ba, C, False))
            new_hat_C.add(PSC_loop(internal_ba, prev_nonhat_C, True))
            prev_nonhat_C = PSC_loop(internal_ba, prev_nonhat_C, False)
            prev_hat_C = new_hat_C
        
        new_hat_C = set()
        for C in prev_stem_hat_C:
            new_hat_C.add(PSC_stem(internal_ba, C, False))
        prev_nonhat_C = PSC_stem(internal_ba, prev_stem_nonhat_C, False)
        prev_hat_C = new_hat_C
        prev_stem_hat_C = new_hat_C
        prev_stem_nonhat_C = prev_nonhat_C
    return tight_ba