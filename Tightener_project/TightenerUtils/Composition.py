'''
    Copyright (c) 2023 Marek Jankola

    This file is part of Tightener.

    Tightener is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tightener is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tightener.  If not, see <http://www.gnu.org/licenses/>.
'''

from InnerRepresentation.NBAUtils import *

def PSC_loop(original_nba : Internal_represenatation, nba : NBA_representation, is_hat : bool) -> NBA_representation:
    not_visited = nba.states.copy()
    visited = set()
    correct_length_mapping = {}
    C = NBA_representation(set(), set(), set(), {})

    while len(not_visited) != 0:
        comp_state0 = not_visited.pop()
        visited.add(comp_state0)
        if comp_state0 in correct_length_mapping:
            origin_comp_state0 = correct_length_mapping[comp_state0]
            if origin_comp_state0 not in nba.transitions:
                continue
            for (origin_comp_state1, cond0, mark0) in nba.transitions[origin_comp_state0]:
                l = comp_state0.loop_states[len(comp_state0.loop_states)-1]
                if str(l) not in original_nba.transitions:
                    continue
                for (t, cond1, mark1) in original_nba.transitions[str(l)]:
                    if origin_comp_state1.is_not_valid_loop_state(str(t)):
                        continue
                    if buddy.bdd_and(cond1, cond0) == buddy.bddfalse:
                        continue
                    comp_state1 = origin_comp_state1.copy()
                    comp_state1.add_loop_state(str(t))

                    # Optimization: if last stem and loop state of comp_state1 equal,
                    # also the last stem state and loop state of comp_state0 must be equal.
                    if comp_state0.is_not_valid_successor(comp_state1):
                        continue

                    if (is_hat & mark1):
                        comp_state1.mark_state()
                    if comp_state1 not in visited:
                        not_visited.add(comp_state1)
                    
                    C.states.add(comp_state1)
                    if comp_state0 not in C.transitions:
                        C.transitions[comp_state0] = set()
                    C.transitions[comp_state0].add((comp_state1, buddy.bdd_and(cond0, cond1), False))

        # EXTENDING BY NEW STATE
        else:
            if ((comp_state0 not in nba.transitions) or (comp_state0.is_not_extensible())):
                continue
            for (comp_state1, cond0, mark0) in nba.transitions[comp_state0]:
                if comp_state1.is_not_extensible():
                    continue
                for s in original_nba.states:
                    if comp_state0.is_not_valid_loop_state(str(s)):
                        continue
                    if str(s) not in original_nba.transitions:
                        continue
                    for (t, cond1, mark1) in original_nba.transitions[str(s)]:
                        if comp_state1.is_not_valid_loop_state(str(t)):
                            continue
                        if buddy.bdd_and(cond0, cond1) == buddy.bddfalse:
                            continue 

                        q = comp_state0.copy()
                        p = comp_state1.copy()
                        if (is_hat & mark1):
                            comp_state1.mark_state()
                            p.mark_state()
                        q.add_loop_state(str(s))
                        p.add_loop_state(str(t))
                        
                        # Optimization: if last stem and loop state of comp_state1 equal,
                        # also the last stem state and loop state of comp_state0 must be equal.
                        if q.is_not_valid_successor(p):
                            continue

                        correct_length_mapping[q] = comp_state0
                        correct_length_mapping[p] = comp_state1

                        if q not in visited:
                            not_visited.add(q)
                        if p not in visited:
                            not_visited.add(p)
                        C.states.add(q)
                        C.states.add(p)
                        if q not in C.transitions:
                            C.transitions[q] = set()
                        C.transitions[q].add((p, buddy.bdd_and(cond0, cond1), False))
    return C

def PSC_stem(original_nba : Internal_represenatation, nba : NBA_representation, is_hat : bool) -> NBA_representation:
    not_visited = nba.states.copy()
    visited = set()
    correct_length_mapping = {}
    C = NBA_representation(set(), set(), set(), {})
    
    while len(not_visited) != 0:
        comp_state0 = not_visited.pop()
        visited.add(comp_state0)
        if comp_state0 in correct_length_mapping:
            origin_comp_state0 = correct_length_mapping[comp_state0]
            if origin_comp_state0 not in nba.transitions:
                continue
            for (origin_comp_state1, cond0, mark0) in nba.transitions[origin_comp_state0]:
                s = comp_state0.stem_states[len(comp_state0.stem_states)-1]
                if str(s) not in original_nba.transitions:
                    continue
                for (r, cond1, mark1) in original_nba.transitions[str(s)]:
                    if origin_comp_state1.is_not_valid_loop_state(str(r)):
                        continue
                    if buddy.bdd_and(cond1, cond0) == buddy.bddfalse:
                        continue
                    comp_state1 = origin_comp_state1.copy()
                    comp_state1.add_stem_state(str(r))

                    # Optimization: if last stem and loop state of comp_state1 equal,
                    # also the last stem state and loop state of comp_state0 must be equal.
                    if comp_state0.is_not_valid_successor(comp_state1):
                        continue

                    if comp_state1 not in C.transitions:
                        C.transitions[comp_state1] = set()
                    if comp_state1 not in visited:
                        not_visited.add(comp_state1)

                    C.states.add(comp_state1)
                    if comp_state0 not in C.transitions:
                        C.transitions[comp_state0] = set()            
                    C.transitions[comp_state0].add((comp_state1, buddy.bdd_and(cond0, cond1), False))
        # EXTENDING BY NEW STATE
        else:
            if ((comp_state0 not in nba.transitions) or (comp_state0.is_not_extensible())):
                continue
            for (comp_state1, cond0, mark0) in nba.transitions[comp_state0]:
                if comp_state1.is_not_extensible():
                    continue
                for s in original_nba.states:
                    if comp_state0.is_not_valid_stem_state(str(s)):
                        continue
                    if str(s) not in original_nba.transitions:
                        continue
                    for (t, cond1, mark1) in original_nba.transitions[str(s)]:
                        if comp_state1.is_not_valid_stem_state(str(t)):
                            continue
                        if buddy.bdd_and(cond0, cond1) == buddy.bddfalse:
                            continue

                        q = comp_state0.copy()
                        p = comp_state1.copy()
                        q.add_stem_state(str(s))
                        p.add_stem_state(str(t))

                        # Optimization: if last stem and loop state of comp_state1 equal,
                        # also the last stem state and loop state of comp_state0 must be equal.
                        if q.is_not_valid_successor(p):
                            continue

                        correct_length_mapping[q] = comp_state0
                        correct_length_mapping[p] = comp_state1

                        if q not in visited:
                            not_visited.add(q)
                        if p not in visited:
                            not_visited.add(p)
                        C.states.add(q)
                        C.states.add(p)
                        if q not in C.transitions:
                            C.transitions[q] = set()
                        C.transitions[q].add((p, buddy.bdd_and(cond0, cond1), False))
    return C