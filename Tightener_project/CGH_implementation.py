# Implementation of CGH+ for future LTL
import z3
import spot

def cgh_formula_to_symbolic(ltl_formula : spot.formula):
    ltl_formula = spot.unabbreviate(ltl_formula, "MGFRW^ei")
    aut = cgh_recursive(ltl_formula)

    return aut

def cgh_recursive(ltl_formula : spot.formula):
    if (str(ltl_formula.kindstr()) == "tt") | (str(ltl_formula.kindstr()) == "ff"):
        T = []
        a = z3.Bool(str(ltl_formula))
        T.append(a)
        F = []
        return [T, F]
    
    if (str(ltl_formula.kindstr()) == "ap"):
        T = []
        F = []
        return [T, F]
    
    children = []
    without_top_op = []
    for child in ltl_formula:
        children.append(cgh_recursive(child))
        without_top_op.append(child)
    
    if str(ltl_formula.kindstr()) == "Not":
        a = z3.Bool(str(ltl_formula).replace('!','#'))
        b = z3.Bool(str(without_top_op[0]).replace('!','#'))
        children[0][0].append(z3.And(z3.Implies(a, z3.Not(b)), z3.Implies(z3.Not(b), a)))
        
        return [children[0][0], children[0][1]]
        
    if str(ltl_formula.kindstr()) == "Or":
        a = z3.Bool(str(ltl_formula).replace('!','#'))
        b = z3.Bool(str(without_top_op[0]).replace('!','#'))
        c = z3.Bool(str(without_top_op[1]).replace('!','#'))

        T = []
        F = []
        for i in range(len(children)):
            T += children[i][0]
            F += children[i][1]
        
        T.append(z3.And(z3.Implies(a, z3.Or(b,c)), z3.Implies(z3.Or(b,c), a)))
        return [T, F]
    
    if str(ltl_formula.kindstr()) == "And":
        a = z3.Bool(str(ltl_formula).replace('!','#'))
        b = z3.Bool(str(without_top_op[0]).replace('!','#'))
        c = z3.Bool(str(without_top_op[1]).replace('!','#'))

        T = []
        F = []
        for i in range(len(children)):
            T += children[i][0]
            F += children[i][1]
        
        T.append(z3.And(z3.Implies(a, z3.And(b,c)), z3.Implies(z3.And(b,c), a)))
        return [T, F]
    
    if str(ltl_formula.kindstr()) == "X":
        a = z3.Bool(str(ltl_formula).replace('!','#'))
        b = z3.Bool(str(without_top_op[0]).replace('!','#') + "\'")

        T = []
        F = []
        for i in range(len(children)):
            T += children[i][0]
            F += children[i][1]
        
        T.append(z3.And(z3.Implies(a, b), z3.Implies(b, a)))
        return [T, F]

    if str(ltl_formula.kindstr()) == "U":
        a = z3.Bool(str(ltl_formula).replace('!','#'))
        next_a = z3.Bool(str(ltl_formula).replace('!','#') + "\'")
        b = z3.Bool(str(without_top_op[0]).replace('!','#'))
        c = z3.Bool(str(without_top_op[1]).replace('!','#'))
        
        T = []
        F = []
        for i in range(len(children)):
            T += children[i][0]
            F += children[i][1]
        
        T.append(z3.And(z3.Implies(a, z3.Or(z3.And(b,next_a), c)), z3.Implies(z3.Or(z3.And(b,next_a), c), a)))
        return [T, F]