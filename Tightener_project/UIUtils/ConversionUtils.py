'''
    Copyright (c) 2023 Marek Jankola

    This file is part of Tightener.

    Tightener is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tightener is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tightener.  If not, see <http://www.gnu.org/licenses/>.
'''


from TightenerUtils.TightenerFunc import *
from UIUtils.PrintUtils import *

def LTLtoTightNBA(formula : str, UseReductions : bool, OutputHOA : bool, sbacc : bool) -> spot.twa_graph:
    try:
        sample_automaton = spot.translate(formula,'Buchi')
    except:
        return None
    
    inner_tight_nba = Tighten_TBA(sample_automaton)
    tight_nba, init_states = create_spot(inner_tight_nba,with_reduction_clemente=UseReductions,state_acceptance=sbacc)
    tight_nba.copy_ap_of(sample_automaton)

    if OutputHOA:
        PrintHOA(tight_nba.to_str('hoa'), init_states)
    else:
        PrintDotFormat(tight_nba.to_str('dot'), init_states)

    return tight_nba

def HOAtoTightNBA(hoa : str, UseReductions : bool, OutputHOA : bool,  sbacc : bool) -> spot.twa_graph:
    try:
        sample_automaton = spot.automaton(hoa)
        sample_automaton = sample_automaton.postprocess('Buchi', "SBAcc")
    except:
        return None
    
    inner_tight_nba = Tighten_TBA(sample_automaton)
    tight_nba, init_states = create_spot(inner_tight_nba,with_reduction_clemente=UseReductions,state_acceptance=sbacc)
    tight_nba.copy_ap_of(sample_automaton)
    
    if OutputHOA:
        PrintHOA(tight_nba.to_str('hoa'), init_states)
    else:
        PrintDotFormat(tight_nba.to_str('dot'), init_states)
    
    return tight_nba

def LTLFiletoTightNBA(file_path : str, UseReductions : bool, OutputHOA : bool,  sbacc : bool):
    with open(file_path) as ltl_file:
        for line in ltl_file:
            LTLtoTightNBA(line, UseReductions, OutputHOA, sbacc)
