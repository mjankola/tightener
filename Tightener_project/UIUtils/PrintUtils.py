'''
    Copyright (c) 2023 Marek Jankola

    This file is part of Tightener.

    Tightener is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tightener is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tightener.  If not, see <http://www.gnu.org/licenses/>.
'''


from TightenerUtils.TightenerFunc import *
import argparse
import sys

def ReadInputOption(argv:List[str]):
    parser = argparse.ArgumentParser()
    parser.add_argument("input", type=str,
                    help="-f <LTL_formula>, -F <file_of_LTL_formulae>, -a <HOA>")
    parser.add_argument("-r", "--reduces", action="store_true",
                    help="Reduces the constructed automaton using quotienting")
    parser.add_argument("-s", "--sbacc", action="store_true",
                    help="The output is state-based Buchi automaton. It still has marks on transitions,"
                     + "but accepting transition lead only to states that can be marked")
    parser.add_argument("-f", "--formula", action="store_true",
                    help="There is an LTL formula on the input")
    parser.add_argument("-F", "--file", action="store_true",
                    help="There is a path to a txt file with LTL formulae")
    parser.add_argument("-a", "--HOA", action="store_true",
                    help="There is a path to .aut file with automaton in HOA format")
    parser.add_argument("-o", "--outputHOA", action="store_true",
                    help="The output is in HOA format including multiple initial states")
    
    try:
        args = parser.parse_args()
    except:
        print("Invalid input !")
        sys.exit(2)
    return args

def PrintHOA(HOA_nba : str, init_states : Set[str]):
    lines_of_HOA = HOA_nba.split("\n")
    lines_of_HOA = lines_of_HOA[:2] + lines_of_HOA[3:]
    
    for init_state in init_states:
        lines_of_HOA = lines_of_HOA[:2] + ["Start: " + str(init_state)] + lines_of_HOA[2:]
     
    HOA_nba = "\n".join(lines_of_HOA)
    print(HOA_nba)


def PrintDotFormat(dot_nba : str, init_states : Set[str]):
    print("Initial states:")

    for init_state in init_states:
        print(init_state)
    
    print(dot_nba)

def PrintFormulaErrorMessage():
    print("LTL Formula on the input is in incorrect format !")

def PrintLTLFileErrorMessage():
    print("Some LTL formula from the file is in incorrect format !")

def PrintHOAErrorMessage():
    print("Given automaton is in incorrect format !")


