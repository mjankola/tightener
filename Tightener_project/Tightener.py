'''
    Copyright (c) 2023 Marek Jankola

    This file is part of Tightener.

    Tightener is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tightener is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tightener.  If not, see <http://www.gnu.org/licenses/>.
'''

from UIUtils.PrintUtils import *
from UIUtils.ConversionUtils import *
import sys

option = ReadInputOption(sys.argv)

if option.formula:
    try:
        LTLtoTightNBA(option.input, option.reduces, option.outputHOA, option.sbacc)
    except:
        PrintFormulaErrorMessage()
        sys.exit(2)
elif option.file:
    try:
        LTLFiletoTightNBA(option.input, option.reduces, option.outputHOA, option.sbacc)
    except:
        PrintFormulaErrorMessage()
        sys.exit(2)
elif option.HOA:
    try:
        HOAtoTightNBA(option.input, option.reduces, option.outputHOA, option.sbacc)
    except:
        PrintHOAErrorMessage()
        sys.exit(2)