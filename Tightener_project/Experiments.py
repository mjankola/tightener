import spot
import z3
import matplotlib.pyplot as plt
from TightenerUtils.TightenerFunc import Tighten_TBA
from InnerRepresentation.NBAUtils import create_spot
from CGH_implementation import cgh_formula_to_symbolic

def create_graphs(graph_title, output_name):
    plt.scatter(result, result1, color="blue", alpha=0.2)
    plt.plot([1,10000], [1,10000], color="red", linestyle = "dashed")
    
    plt.grid()
    # naming the x axis
    plt.xlabel('CGH')
    # naming the y axis
    plt.ylabel('Tightener')
    # giving a title to my graph
    plt.title(graph_title)
    plt.xscale('log')
    plt.yscale('log')

    # function to show the plot
    plt.savefig(output_name + '.png')


    plt.scatter(result, result2, color="blue", alpha=0.2)
    plt.plot([1,10000], [1,10000], color="red", linestyle = "dashed")
    
    plt.grid()
    # naming the x axis
    plt.xlabel('CGH')
    # naming the y axis
    plt.ylabel('Tightener')
    # giving a title to my graph
    plt.title(graph_title + '(SA)')
    plt.xscale('log')
    plt.yscale('log')

    # function to show the plot
    plt.savefig(output_name + '_SA' + '.png')

def run_CGH(file):
    result = []
    i = 0
    with open(file) as ltl_file:
        for ltl_formula in ltl_file:
            if ltl_formula[0] == 'T':
                continue
            formula = spot.formula(ltl_formula)

            sample_automaton = cgh_formula_to_symbolic(formula)
            s = z3.Solver()
            tt = z3.Bool('1')
            ff = z3.Bool('0')
            num_states = 0
            s.append(sample_automaton[0])
            s.append(sample_automaton[1])
            s.add(tt == True)
            s.add(ff == False)

            while s.check() == z3.sat:  
                m = s.model()
                solution = []
                for i in m:
                    a = z3.Bool(str(i))
                    solution.append(a != m[i])
                s.add(z3.Or(solution))
                num_states += 1
            
            result.append(num_states)
            print(str(num_states))
    print(result)
    return result

def run_Tightener(file):
    result1 = []
    result2 = []
    with open(file) as ltl_file:
        for ltl_formula in ltl_file:
            if ltl_formula[0] == 'T':
                continue
            formula = spot.formula(ltl_formula)

            sample_automaton = spot.translate(formula,  'Buchi')
            tight_nba = Tighten_TBA(sample_automaton)
            tight_tba_reduced, _ = create_spot(tight_nba, False, True)
            tight_ba_reduced, _ = create_spot(tight_nba, False, True, False, True)

            result1.append(tight_tba_reduced.num_states())
            result2.append(tight_ba_reduced.num_states())
            print(str(sample_automaton.num_states()) + ', ' + str(tight_tba_reduced.num_states()) + ', ' + str(tight_ba_reduced.num_states()) + ', ' + ltl_formula)
    print(result1)
    print(result2)
    return (result1, result2)


print("Starting the evauluation on random dataset:")
print("===========================================")
print("Starting evaluation of Tightener:")
print("===========================================")
print("Number of states after translation | Tightening + Reduction | Tightening + Reduction + State-based")
(result1, result2) = run_Tightener('ltlDataSet_random.txt')
print("Starting evaluation of CGH:")
print("===========================================")
print("Number of states after cgh translation:")
result = run_CGH('ltlDataSet_random.txt')
create_graphs('CGH vs Tightener on the random dataset', 'Random_Graph')
print("===========================================")
print("Starting the evauluation on random dataset:")
print("===========================================")
print("Starting evaluation of Tightener:")
print("===========================================")
print("Number of states after translation | Tightening + Reduction | Tightening + Reduction + State-based")
(result1, result2) = run_Tightener('ltlDataSet_patterns.txt')
print("Number of states after cgh translation:")
result = run_CGH('ltlDataSet_patterns.txt')
create_graphs('CGH vs Tightener on the literature dataset', 'Literature_Graph')
