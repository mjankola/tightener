FROM continuumio/miniconda3

WORKDIR .

# Create the environment:
COPY tightener_env.yml .
RUN conda env create -f tightener_env.yml

# Make RUN commands use the new environment:
SHELL ["conda", "run", "-n", "tightener_env", "/bin/bash", "-c"]

# Demonstrate the environment is activated:
RUN echo "Make sure spot is installed:"
RUN python -c "import spot"
RUN echo "Make sure z3 is installed:"
RUN python -c "import z3"

# The code to run when container is started:
COPY Tightener_project .
ENTRYPOINT ["conda", "run", "--no-capture-output", "-n", "tightener_env", "python", "Tightener.py"]

